Thread.abort_on_exception = true

class Server
  attr_reader :channel

  def initialize(channel)
    @channel = channel
  end

  def run
    Socket.udp_server_loop(23332) do |msg|
      begin
        Thread.new { channel.put JSON.parse msg, symbolize_names: true }
      rescue StandardError => e
        puts e.message, e.backtrace.join("\n")
      end
    end
  end

  # def process(msg)
  #   params = JSON.parse msg, symbolize_names: true
  #   puts "received message: #{params.inspect}"
  #   @grid.apply params
  #   p "S #{@grid.cells[1].inspect}"
  # end
end
