class Grid
  attr_reader :rows, :cols, :cells, :size, :margin, :screen, :index

  def initialize(cols, rows, size = 40, margin = 40)
    @cols = cols * (size + margin) > WIDTH ? WIDTH / (size + margin) : cols
    @rows = rows * (size + margin) > HEIGHT ? HEIGHT / (size + margin) : rows
    @margin = margin
    @size = size

    @screen = OpenStruct.new(offset_y: margin / 2 + (HEIGHT - (size + margin) * rows) / 2,
                             offset_x: margin / 2 + (WIDTH - (size + margin) * cols) / 2)

    @cells = []
    @index = {}

    rows.times do |row|
      cols.times do |col|
        x = @screen.offset_x + (size + margin) * col
        y = @screen.offset_y + (size + margin) * row

        cells << Circle.new(x: x, y: y, radius: size / 2, sectors: 32, color: "#000000")
        @index[[col,row]] = cells.last
      end
    end
  end

  def [](x, y)
    index[[x,y]]
  end

  def col(n)
    n = n % cols
    index.select { |(col,_), _| col == n }.values
  end

  def row(n)
    n = n % rows
    index.select { |(_,row), _| row == n }.values
  end

  def apply(params = {})
    cell = @cells[params[:i] % @cells.size]
    cell.color = params[:color] if params[:color]
    cell.radius = params[:size] if params[:size]
    cell.opacity = params[:opacity].to_i / 100.0 if params[:opacity]
    cell.sectors = params[:sides] if params[:sides]
  end
end
