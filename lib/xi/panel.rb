module Xi
  module Panel
    class Client
      include Logger

      attr_reader :host, :port, :socket
      def initialize(host, port)
        @host = host
        @port = port
        @socket = UDPSocket.new
        socket.connect host, port
      end

      def send_command(args)
        debug(args)
        socket.send JSON.dump(args), 0
      end
    end

    class Stream < ::Xi::Stream
      attr_reader :client

      DEFAULT_PARAMS = {}

      def initialize(name, clock, server: 'localhost', port: 23332)
        super
        @client = Client.new(server, port)
      end

      # Esto no se si está bien :P
      def transform_state
        client.send_command @state
      end
    end
  end
end
