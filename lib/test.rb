def test(grid)
  tick = 0
  x = 0
  y = 0
  part = grid.row(15).shuffle.take(3 + rand * 5)

  update do
    if tick % [5].sample == 0
      tick = 0

      grid[x,y].color = "white"
      x = (x + 1) % grid.cols

      if x == 0
        start = rand(15)
        y = (y + 1) % grid.rows
        part = grid.row(15 - y)[start..(start + 5)]
      end

      grid[x,y].color = ["lime", "fuchsia", "aqua",  "teal", "white", "purple"].sample
      grid[x,y].radius = 30
      grid[x,y].opacity = 1
      grid[x,y].sectors = [3,4,5,6].sample

      # s = sides.next
      part.each do |c|
        c.sectors = [3,4,5,6].sample
        c.radius = 30 * (0.1 + rand)
        c.opacity = 1
        c.color = ["lime", "fuchsia", "aqua",  "teal", "white", "purple"].sample
      end

      # grid.col(x).each do |c|
      #   c.sectors = [3,4,5,6].sample
      #   c.radius = 30 * (0.1 + rand)
      #   c.opacity = 1
      #end

      grid.cells.each { |c|
        c.opacity -= 0.05
        c.radius -= 0.5 if c.radius > 0
      }
    end

    tick += 1
  end
end
