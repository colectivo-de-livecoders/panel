#!/usr/bin/ruby -w
require 'ruby2d'
STDOUT.sync = true
WIDTH = 1920 / 2
HEIGHT = 1080

require 'ostruct'
require 'socket'
require 'json'
require 'cod'

require_relative './lib/grid'
require_relative './lib/server'
require_relative './lib/test'


#set fullscreen: true
set title: "panel"
set width: WIDTH
set height: HEIGHT

grid = Grid.new 12, 14, 30

channel = Cod.pipe

# server para recibir eventos desde Xi via UDP
# tiene que correr en otro proceso porque parece que en un thread
# se pelea mucho con el loop de rendering de ruby2D y se traba todo
# ¯\_(ツ)_/¯
fork { Server.new(channel).run }

# Como el server corre en otro proceso, no tiene acceso a la instancia de Grid definida arriba.
# Este thread recibe los eventos que recibe el server en el otro proceso y los aplica a la grid.
# No se puede hacer un channel.get en el loop principal porque es bloqueante
# ¯\_(ツ)_/¯
Thread.new do
  loop { grid.apply channel.get }
end

# update { sleep 0.01 }

#test grid

show
