para probarlo en una pestaña correr:

```
bundle exec ruby panel.rb
```
en otra

```
bundle exec xi
```

y en xi:

```ruby
require_relative "./lib/xi/panel.rb"
a = Xi::Panel::Stream.new(:panel, Xi.default_clock)
a.set(
  i: (0..167).p(1/66).rand,
  opacity: (10..90).p(1/66).rand,
  color: ["random", "#000000", "#000000"],
  size: (2..180).p(1/40).rand,
  sides: P.rand([4])
)

```

* `i`: el indice de la figura en el array dentro de Grid. Podría haber otras formas de referenciar las celdas. (columnas? filas? tipo acordes, para agarrar varias al mismo tiempo?)
* el resto son bastante directos :P (https://www.ruby2d.com/learn/shapes/#circles)
