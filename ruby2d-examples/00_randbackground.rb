#!/usr/bin/ruby -w
# Reaction game!

require 'ruby2d'

STDOUT.sync = true
WIDTH = 1920
HEIGHT = 1080

class Grid
  attr_reader :width, :height, :cells

  def initialize(width, height)
    @width = width
    @height = height
    @cells = []
    width.times do |col|
      height.times do |row|
        cells << Square.new
      end
    end
  end
end


set fullscreen: true
set title: "panel"
set width: WIDTH
set height: HEIGHT


tick = 0

update do
  if tick % [5,10,20,30].sample == 0
    set background: 'random'
  end
  tick += 1
end

show
