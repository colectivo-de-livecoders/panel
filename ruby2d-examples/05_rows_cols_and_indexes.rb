#!/usr/bin/ruby -w
# Reaction game!

require 'ruby2d'
require 'ostruct'

STDOUT.sync = true
WIDTH = 1920
HEIGHT = 1080

class Grid
  attr_reader :rows, :cols, :cells, :size, :margin, :screen, :index

  def initialize(cols, rows, size = 40, margin = 40)
    @cols = cols * (size + margin) > WIDTH ? WIDTH / (size + margin) : cols
    @rows = rows * (size + margin) > HEIGHT ? HEIGHT / (size + margin) : rows
    @margin = margin
    @size = size

    @screen = OpenStruct.new(offset_y: margin / 2 + (HEIGHT - (size + margin) * rows) / 2,
                             offset_x: margin / 2 + (WIDTH - (size + margin) * cols) / 2)

    @cells = []

    rows.times do |row|
      cols.times do |col|
        x = @screen.offset_x + (size + margin) * col
        y = @screen.offset_y + (size + margin) * row

        cells << Circle.new(x: x, y: y, radius: size / 2, sectors: 32, color: "white")
      end
    end

    @index = @cells.map.with_index { |c, i| { i => c } }.reduce(&:merge)
  end

  def [](x, y)
    cells[(y % rows) * cols + x % cols]
  end

  def col(n)
    n = n % cols

    index.select { |i, c| (i - n) % cols == 0 }.values
  end

  def row(n)
    n = n % rows

    index.select { |i, c| i >= n * cols && i < (n + 1 ) * cols }.values
  end
end

set fullscreen: true
set title: "panel"
set width: WIDTH
set height: HEIGHT

tick = 0
grid = Grid.new 20, 15, 30
sides = [3,4,5,6].sample
x = 0
y = 0

update do
  if tick % [5].sample == 0
    tick = 0

    grid[x,y].color = "white"
    x = (x + 1) % grid.cols
    y = (y + 1) % grid.rows if x == 0
    grid[x,y].color = ["lime", "fuchsia", "aqua",  "teal"].sample

    s = sides.next
    grid.row(x).each do |c|
      c.sectors = [3,4,5,6].sample
      c.radius = 30 * (0.1 + rand)
    end
  end

  tick += 1
end

show
