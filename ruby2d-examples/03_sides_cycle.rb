#!/usr/bin/ruby -w
# Reaction game!

require 'ruby2d'
require 'ostruct'

STDOUT.sync = true
WIDTH = 1920
HEIGHT = 1080

class Grid
  attr_reader :rows, :cols, :cells, :size, :margin, :screen

  def initialize(cols, rows, size = 40, margin = 40)
    @cols = cols
    @rows = rows
    @margin = margin
    @size = size
    @screen = OpenStruct.new(offset_y: (HEIGHT - (size + margin) * rows) / 2,
                             offset_x: (WIDTH - (size + margin) * cols) / 2)

    p @screen
    @cells = []
    rows.times do |row|
      cols.times do |col|
        x = @screen.offset_x + (size + margin) * col
        y = @screen.offset_y + (size + margin) * row

        cells << Circle.new(x: x, y: y, radius: size / 2, sectors: 4, color: "white")
      end
    end
  end
end


set fullscreen: true
set title: "panel"
set width: WIDTH
set height: HEIGHT

tick = 0
grid = Grid.new 24, 12
sides = [3,4,6,8].cycle

update do
  if tick % 10 == 0
    s = sides.next
    grid.cells.each { |c| c.sectors = s  }
  end
  tick += 1
end



show
