require 'ruby2d'

set title: "test"

set width:  1920
set height: 1080
set fullscreen: true

rows = 11
cols = 22
size = 40
margin = 40

cells = []
on = []

rows.times do |row|
  cols.times do |col|
    x = 100 + (size + margin) * col
    y = 100 + (size + margin) * row

    cells << Circle.new(x: x, y: y, radius: size / 2, sectors: 3, color: "white")
  end
end

fps_label = Text.new('0')


tick = 0

update do
  fps_label.text = get :fps

  if tick % 20 == 0
    cells.each { |c| c.opacity -= 0.1 }
    tick = 0

    cells.sample(cells.size / 4).each { |c| c.color = "white"; c.sectors = [3,4,6].sample }
  end

  tick += 1
end

show
