#!/usr/bin/ruby -w
# Reaction game!

require 'ruby2d'
require 'ostruct'

STDOUT.sync = true
WIDTH = 1920
HEIGHT = 1080

class Grid
  attr_reader :rows, :cols, :cells, :size, :margin, :screen

  def initialize(cols, rows, margin = 40)
    @cols = cols
    @rows = rows
    @margin = margin


    @cell = OpenStruct.new(h: HEIGHT / rows - margin,
                           w: WIDTH / cols - margin)

    @screen = OpenStruct.new(offset_y: margin / 2 + (HEIGHT - (HEIGHT / rows) * rows) / 2,
                             offset_x: margin / 2 + (WIDTH - (WIDTH / cols) * cols) / 2)

p @cell, @screen
    @cells = []
    rows.times do |row|
      cols.times do |col|
        x = @screen.offset_x + (@cell.w + margin) * col
        y = @screen.offset_y + (@cell.h + margin) * row

        cells << Rectangle.new(x: x, y: y, width: @cell.w, height: @cell.h, color: "white")
      end
    end
  end
end


set fullscreen: true
set title: "panel"
set width: WIDTH
set height: HEIGHT

tick = 0
grid = Grid.new 10, 10

update do
  #grid.cells.each { |c| c.color = ["white", "black"].sample if tick % 10 == 0 }
  tick += 1
end



show
