#!/usr/bin/ruby -w
require 'ruby2d'
STDOUT.sync = true
WIDTH = 1920
HEIGHT = 1080

require 'ostruct'
require 'socket'
require 'json'

require_relative './lib/grid'
require_relative './lib/server'
require_relative './lib/panel'

grid = Grid.new 20, 15, 30

Thread.new { Server.new(grid).run }

Panel.new(grid).run
